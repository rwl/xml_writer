# XMLWriter for Dart

A native and full dart implementation of the classic XMLWriter class.
The API is complete, flexible and tolerant.
XML is still valid.

## Contributors

  * [Nicolas Thouvenin](https://github.com/touv) 
  * [Anton Zem](https://github.com/AlgoTrader)
  * [Chip Lee](https://github.com/chipincode)
  * [Peecky](https://github.com/peecky)

# Examples

## Basic
```dart
	var xw = new XMLWriter();
	xw.startDocument();
	xw.startElement('root');
	xw.writeAttribute('foo', 'value');
	xw.text('Some content');
	xw.endDocument();

	print(xw.toString());
```
Output:
	
	<?xml version="1.0"?>
	<root foo="value">Some content</root>
	
Tip: If you want your XML **indented** use `new XMLWriter(true)`.
	
## Chaining
```dart
	var xw = new XMLWriter();
	xw.startDocument()..startElement('root')..writeAttribute('foo', 'value')..writeElement('tag', 'Some content');

	print(xw.toString());
```
Output:
	
	<?xml version="1.0"?>
	<root foo="value"><tag>Some content</tag></root>
	
## Tolerant
```dart
	var xw = new XMLWriter();
	xw.startElement('root')..writeAttribute('foo', 'value')..text('Some content');

	print(xw.toString());
```
Output:
	
	<root foo="value">Some content</root>	

## Extensible
```dart
	var ws = fs.createWriteStream('/tmp/foo.xml');
	ws.onClose.listen(() {
			print(fs.readFileSync('/tmp/foo.xml', 'UTF-8'));
	});
	var xw = new XMLWriter(false, (String string, String encoding) { 
			ws.write(string, encoding);
	});
	xw.startDocument('1.0', 'UTF-8')..startElement(() {
		return 'root';
	})..text(() {
		return 'Some content';
	});
	ws.end();
```

Output:
	
	<?xml version="1.0" encoding="UTF-8"?>
	<root>Some content</root>
		
# Also

* https://github.com/touv/node-xml-writer
* https://github.com/minchenkov/simple-xml-writer
* https://github.com/wezm/node-genx

# License

[MIT/X11](./LICENSE)
