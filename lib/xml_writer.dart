library xml_writer;

//isFalse(s) {
//  return s is! num && !s;
//}
//
//strval(s) {
//  if (s is String) {
//    return s;
//  } else if (s is num) {
//    return s.toString();
//  } else if (s is Function) {
//    return s();
//  } else if (s is XMLWriter) {
//    return s.toString();
//  } else {
//    throw new Exception('Bad Parameter');
//  }
//}

class XMLWriter {

  RegExp name_regex;
  bool indent;
  String output;
  List<Map> stack;
  int tags;
  int attributes;
  int attribute;
  int texts;
  int comment;
  int dtd;
  String root;
  int pi;
  int cdata;
  bool started_write;
  Function writer;
  String writer_encoding;

  XMLWriter([bool indent=false, Function callback=null]) {

//    if (!(this instanceof XMLWriter)) {
//        return new XMLWriter();
//    }

    name_regex = new RegExp(r"[_:A-Za-z][-._:A-Za-z0-9]*");
    this.indent = indent ? true : false;
    output = '';
    stack = [];
    tags = 0;
    attributes = 0;
    attribute = 0;
    texts = 0;
    comment = 0;
    dtd = 0;
    root = '';
    pi = 0;
    cdata = 0;
    started_write = false;
    writer;
    writer_encoding = 'UTF-8';

    if (callback != null) {
      writer = callback;
    } else {
      writer = (s, e) {
        output += s;
      };
    }
  }

//XMLWriter.prototype = {
  toString() {
    flush();
    return output;
  }

  void indenter() {
    if (indent) {
      write('\n');
      for (var i = 1; i < tags; i++) {
        write('    ');
      }
    }
  }

  /// Write text.
  void write(String s) {
//    for (var i = 0; i < arguments.length; i++) {
//      writer(arguments[i], writer_encoding);
//    }
    writer(s, writer_encoding);
  }

  void writeAll(List<String> s) {
    s.forEach((String ss) {
      writer(ss, writer_encoding);
    });
  }


  void flush() {
    for (var i = tags; i > 0; i--) {
      endElement();
    }
    tags = 0;
  }

  /// Create document tag.
  void startDocument([String version='1.0', String encoding=null, bool standalone=false]) {
    if (tags != 0 || attributes != 0) {
      return;
    }

    startPI('xml');
    startAttribute('version');
    text(version);
    endAttribute();
    if (encoding != null) {
      startAttribute('encoding');
      text(encoding);
      endAttribute();
      writer_encoding = encoding;
    }
    if (standalone) {
      startAttribute('standalone');
      text("yes");
      endAttribute();
    }
    endPI();
    if (!indent) {
      write('\n');
    }
  }

  /// End current document.
  void endDocument() {
    if (attributes != 0) {
      endAttributes();
    }
  }

  // Element

  /// Write full element tag.
  void writeElement(String name, String content) {
    this..startElement(name)
      ..text(content)
      ..endElement();
  }

  /// Write full namespaced element tag.
  void writeElementNS(prefix, name, uri, content) {
    if (content == null || content.length == 0) {
      content = uri;
    }
    this..startElementNS(prefix, name)//, uri)
      ..text(content)
      ..endElement();
  }

  /// Create start element tag.
  void startElement(String name) {
    //name = strval(name);
    if (!name_regex.hasMatch(name)) {
      throw new Exception('Invalid Parameter');
    }
    if (tags == 0 && root != null && root.length != 0 && root != name) {
      throw new Exception('Invalid Parameter');
    }
    if (attributes != 0) {
      endAttributes();
    }
    ++tags;
    texts = 0;
    if (stack.length > 0) {
      stack[stack.length - 1]['containsTag'] = true;
    }

    stack.add({
      'name': name,
      'tags': tags
    });
    if (started_write) {
      indenter();
    }
    writeAll(['<', name]);
    startAttributes();
    started_write = true;
  }

  /// Create start namespaced element tag.
  void startElementNS(String prefix, String name) {//, String uri) {
//    prefix = strval(prefix);
//    name = strval(name);

    if (!name_regex.hasMatch(prefix)) {
      throw new Exception('Invalid Parameter');
    }
    if (!name_regex.hasMatch(name)) {
      throw new Exception('Invalid Parameter');
    }
    if (attributes != 0) {
      endAttributes();
    }
    ++tags;
    texts = 0;
    if (stack.length > 0) {
      stack[stack.length - 1]['containsTag'] = true;
    }

    stack.add({
      'name': prefix + ':' + name,
      'tags': tags
    });
    if (started_write) {
      indenter();
    }
    writeAll(['<', '$prefix:$name']);
    startAttributes();
    started_write = true;
  }

  /// End current element.
  void endElement() {
    if (tags == 0) {
      return;
    }
    Map t = stack.removeLast();
    if (attributes > 0) {
      if (attribute != 0) {
        if (texts != 0) {
          endAttribute();
        }
        endAttribute();
      }
      write('/');
      endAttributes();
    } else {
      if (t['containsTag'] == true) {
        indenter();
      }
      writeAll(['</', t['name'], '>']);
    }
    --tags;
    texts = 0;
  }

  // Attributes

  /// Write full attribute.
  void writeAttribute(String name, String content) {
//    if (content is Function) {
//      content = content();
//    }
//    if (isFalse(content)) {
//      return this;
//    }
    if (content == null) {
      return;
    }
    this..startAttribute(name)..text(content)..endAttribute();
  }

  /// Write full namespaced attribute.
  void writeAttributeNS(String prefix, String name, String uri, [String content]) {
    if (content == null) {
      content = uri;
    }
//    if (content is Function) {
//      content = content();
//    }
//    if (isFalse(content)) {
//      return this;
//    }
    this..startAttributeNS(prefix, name, uri)
      ..text(content)
      ..endAttribute();
  }

  void startAttributes() {
    attributes = 1;
  }

  void endAttributes() {
    if (attributes == 0) {
      return;
    }
    if (attribute != 0) {
      endAttribute();
    }
    attributes = 0;
    attribute = 0;
    texts = 0;
    write('>');
    return;
  }

  /// Create start attribute.
  void startAttribute(String name) {
    //name = strval(name);
    if (!name_regex.hasMatch(name)) {
      throw new Exception('Invalid Parameter');
    }
    if (attributes == 0 && pi == 0) {
      return;
    }
    if (attribute != 0) {
      return;
    }
    attribute = 1;
    writeAll([' ', name, '="']);
  }

  /// Create start namespaced attribute.
  void startAttributeNS(String prefix, String name, String uri) {
//    prefix = strval(prefix);
//    name = strval(name);

    if (!name_regex.hasMatch(prefix)) {
      throw new Exception('Invalid Parameter');
    }
    if (!name_regex.hasMatch(name)) {
      throw new Exception('Invalid Parameter');
    }
    if (attributes == 0 && pi == 0) {
      return;
    }
    if (attribute != 0) {
      return;
    }
    attribute = 1;
    writeAll([' ', prefix + ':' + name, '="']);
  }

  /// End attribute.
  void endAttribute() {
    if (attribute == 0) {
      return;
    }
    attribute = 0;
    texts = 0;
    write('"');
  }

  void text(String content) {
    //content = strval(content);
    if (tags == 0 && comment == 0 && pi == 0 && cdata == 0) {
      return;
    }
    if (attributes != 0 && attribute != 0) {
      ++texts;
      write(content.replaceAll(r"&", '&amp;').replaceAll(r"<", '&lt;').replaceAll(">", '&gt;').replaceAll(r'"', '&quot;'));
      return;
    } else if (attributes != 0 && attribute == 0) {
      endAttributes();
    }
    if (comment != 0 || cdata != 0) {
      write(content);
    } else {
      write(content.replaceAll(r'&', '&amp;').replaceAll(r'<', '&lt;').replaceAll(r'>', '&gt;'));
    }
    ++texts;
    started_write = true;
  }

  // Comments

  /// Write full comment tag.
  void writeComment(String content) {
    this..startComment()
      ..text(content)
      ..endComment();
  }

  /// Create start comment.
  void startComment() {
    if (comment != 0) {
      return;
    }
    if (attributes != 0) {
      endAttributes();
    }
    indenter();
    write('<!--');
    comment = 1;
    started_write = true;
  }

  /// Create end comment.
  void endComment() {
    if (comment == 0) {
      return;
    }
    write('-->');
    comment = 0;
  }

  // DocType

  /// Write a DocType.
  void writeDocType(String name, [String pubid, String sysid, String subset]) {
    this..startDocType(name, pubid, sysid, subset)..endDocType();
  }

  /// Create start DocType tag.
  void startDocType(String name, [String pubid, String sysid, String subset]) {
    if (dtd != 0 || tags != 0) return;

//    name = strval(name);
//    pubid = pubid ? strval(pubid) : pubid;
//    sysid = sysid ? strval(sysid) : sysid;
//    subset = subset ? strval(subset) : subset;

    if (!name_regex.hasMatch(name)) {
      throw new Exception('Invalid Parameter');
    }
    if (pubid != null && !new RegExp(r'^[\w\-][\w\s\-\/\+\:\.]*').hasMatch(pubid)) {
      throw new Exception('Invalid Parameter');
    }
    if (sysid != null && !new RegExp(r'^[\w\.][\w\-\/\\\:\.]*').hasMatch(sysid)) {
      throw new Exception('Invalid Parameter');
    }
    if (subset != null && !new RegExp(r'[\w\s\<\>\+\.\!\#\-\?\*\,\(\)\|]*').hasMatch(subset)) {
      throw new Exception('Invalid Parameter');
    }

    pubid = pubid != null ? ' PUBLIC "' + pubid + '"' : (sysid != null) ? ' SYSTEM' : '';
    sysid = sysid != null ? ' "' + sysid + '"' : '';
    subset = subset != null ? ' [' + subset + ']' : '';

    if (started_write) {
      indenter();
    }
    writeAll(['<!DOCTYPE ', name, pubid, sysid, subset]);
    root = name;
    dtd = 1;
    started_write = true;
  }

  /// End current DocType.
  void endDocType() {
    if (dtd == 0) return;
    write('>');
  }

  // Processing Instruction

  /// Writes a PI.
  void writePI(String name, String content) {
    this..startPI(name)
      ..text(content)
      ..endPI();
  }

  /// Create start PI tag.
  void startPI(String name) {
    //name = strval(name);
    if (!name_regex.hasMatch(name)) {
      throw new Exception('Invalid Parameter');
    }
    if (pi != 0) return;
    if (attributes != 0) {
      endAttributes();
    }
    if (started_write) {
      indenter();
    }
    writeAll(['<?', name]);
    pi = 1;
    started_write = true;
  }

  /// End current PI.
  void endPI() {
    if (pi == 0) return;
    write('?>');
    pi = 0;
  }

  // CData

  /// Write full CDATA tag.
  void writeCData(String content) {
    this..startCData()
      ..text(content)
      ..endCData();
  }

  /// Create start CDATA tag.
  void startCData() {
    if (cdata != 0) return;
    if (attributes != 0) {
      endAttributes();
    }
    indenter();
    write('<![CDATA[');
    cdata = 1;
    started_write = true;
  }

  /// End current CDATA.
  void endCData() {
    if (cdata == 0) return;
    write(']]>');
    cdata = 0;
  }

  void writeWriter(XMLWriter writer) {
    writeRaw(writer.toString());
  }

  /// Write a raw XML text.
  void writeRaw(String content) {
    //content = strval(content);
    if (tags == 0 && comment == 0 && pi == 0 && cdata == 0) {
      return;
    }
    if (attributes == 0 && attribute == 0) {
      ++texts;
      write(content.replaceAll('&', '&amp;').replaceAll('"', '&quot;'));
      return;
    } else if (attributes != 0 && attribute == 0) {
      endAttributes();
    }
    ++texts;
    write(content);
    started_write = true;
  }
}
