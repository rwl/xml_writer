library xml_writer.test.attributes;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testAttributes() {
  group('attributes', () {
    XMLWriter xw;
    setUp(() {
    	xw = new XMLWriter();
    	//callback();
    });
    test('t01', () {
    	xw.startAttribute('key');
    	xw.text('value');
    	xw.endAttribute();
    	expect(xw.toString(), equals(''));
    });
    test('t02', () {
    	xw.startElement('tag');
    	xw.startAttribute('key');
    	xw.text('value');
    	xw.endAttribute();
    	expect(xw.toString(), equals('<tag key="value"/>'));
    });
    test('t03', () {
    	xw.startElement('tag');
    	xw.startAttribute('key1');
    	xw.text('value');
    	xw.endAttribute();
    	xw.startAttribute('key2');
    	xw.text('value');
    	xw.endAttribute();
    	expect(xw.toString(), equals('<tag key1="value" key2="value"/>'));
    });
    test('t04', () {
    	xw.startElement('tag');
    	xw.startAttribute('key1');
    	xw.startAttribute('key2');
    	xw.text('value');
    	xw.endAttribute();
    	expect(xw.toString(), equals('<tag key1="value"/>'));
    });
    test('t05', () {
    	xw.startElement('tag');
    	xw.startAttribute('key1');
    	xw.startElement('tag');
    	xw.text('value');
    	expect(xw.toString(), equals('<tag key1=""><tag>value</tag></tag>'));
    });
    test('t06', () {
    	xw..startElement('tag')..writeAttribute('key', 'value')..endElement();
    	expect(xw.toString(), equals('<tag key="value"/>'));
    });
    test('t06', () {
    	xw..startElement('tag')..writeAttribute('key', '"< & >"')..endElement();
    	expect(xw.toString(), equals('<tag key="&quot;&lt; &amp; &gt;&quot;"/>'));
    });
    test('t07', () {
      xw.startElement('tag');
      //xw.writeAttribute('key1', false);
      xw.writeAttribute('key2', null);
      //xw.writeAttribute('key3', undefined);
      expect(xw.toString(), equals('<tag/>'));
    });
  });
}
