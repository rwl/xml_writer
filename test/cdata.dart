library xml_writer.test.cdata;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testCData() {
  group('cdata', () {
    XMLWriter xw;
    setUp(() {
    	xw = new XMLWriter();
    	//callback();
    });
    test('t01', () {
    	xw.startCData();
    	xw.text('fake');
    	xw.endCData();
    	expect(xw.toString(), equals('<![CDATA[fake]]>'));
    });
    test('t02', () {
    	xw.startElement('tag');
    	xw.startCData();
    	xw.text('fake');
    	xw.endCData();
    	xw.text('value');
    	expect(xw.toString(), equals('<tag><![CDATA[fake]]>value</tag>'));
    });
    test('t03', () {
    	xw.startElement('tag');
    	xw.startCData();
    	xw.startCData();
    	xw.text('fake');
    	xw.endCData();
    	xw.text('value');
    	xw.endCData();
    	xw.startCData();
    	xw.text('fake');
    	xw.endCData();

    	expect(xw.toString(), equals('<tag><![CDATA[fake]]>value<![CDATA[fake]]></tag>'));
    });
    test('t04', () {
    	xw.startElement('tag');
    	xw.startCData();
    	xw.startElement('tag');
    	xw.text('value');
    	xw.endElement();
    	xw.endCData();
    	xw.endElement();
    	expect(xw.toString(), equals('<tag><![CDATA[<tag>value</tag>]]></tag>'));
    });
    test('t06', () {
    	xw.writeCData('value');
    	expect(xw.toString(), equals('<![CDATA[value]]>'));
    });
  });
}