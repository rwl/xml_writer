library xml_writer.test.comments;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testComments() {
  group('comments', () {
    XMLWriter xw;
    setUp(() {
    	xw = new XMLWriter();
    	//callback();
    });
    test('t01', () {
    	xw.startComment();
    	xw.text('fake');
    	xw.endComment();
    	expect(xw.toString(), equals('<!--fake-->'));
    });
    test('t02', () {
    	xw.startElement('tag');
    	xw.startComment();
    	xw.text('fake');
    	xw.endComment();
    	xw.text('value');
    	expect(xw.toString(), equals('<tag><!--fake-->value</tag>'));
    });
    test('t03', () {
    	xw.startElement('tag');
    	xw.startComment();
    	xw.startComment();
    	xw.text('fake');
    	xw.endComment();
    	xw.text('value');
    	xw.endComment();
    	xw.startComment();
    	xw.text('fake');
    	xw.endComment();

    	expect(xw.toString(), equals('<tag><!--fake-->value<!--fake--></tag>'));
    });
    test('t04', () {
    	xw.startElement('tag');
    	xw.startComment();
    	xw.startElement('tag');
    	xw.text('value');
    	xw.endElement();
    	xw.endComment();
    	xw.endElement();
    	expect(xw.toString(), equals('<tag><!--<tag>value</tag>--></tag>'));
    });
    test('t06', () {
    	xw.writeComment('value');
    	expect(xw.toString(), equals('<!--value-->'));
    });
    test('t06', () {
    	xw.writeComment('<test>');
    	expect(xw.toString(), equals('<!--<test>-->'));
    });
  });
}