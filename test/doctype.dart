library xml_writer.test.doctype;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testDocType() {
  group('doctype', () {
    XMLWriter xw;
    setUp(() {
        xw = new XMLWriter();
        //callback();
    });
    test('t01', () {
        xw.writeDocType('foo');
        expect(xw.toString(), equals('<!DOCTYPE foo>'));
    });
    test('t02', () {
        xw.writeDocType('foo', null, 'http://localhost/foo');
        expect(xw.toString(), equals('<!DOCTYPE foo SYSTEM "http://localhost/foo">'));
    });
    test('t03', () {
        xw.writeDocType('foo', null, null, '<!ELEMENT foo><!ATTLIST foo DocVersion (1) #REQUIRED>');
        expect(xw.toString(), equals('<!DOCTYPE foo [<!ELEMENT foo><!ATTLIST foo DocVersion (1) #REQUIRED>]>'));
    });
    test('t04', () {
        xw.writeDocType('html', "-//W3C//DTD XHTML 1.1//EN", "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd");
        expect(xw.toString(), equals('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">'));
    });
    test('t05', () {
        expect(() {
            xw.writeDocType('foo', null, null, null);
            xw.startElement('bar');
        }, throws);
    });
    test('t06', () {
        xw.startElement('foo');
        xw.writeDocType('foo', null, null, null);
        expect(xw.toString(), equals('<foo/>'));
    });
  });
}