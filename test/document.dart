library xml_writer.test.document;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testDocument() {
  group('document', () {
    XMLWriter xw;
    setUp(() {
    	xw = new XMLWriter();
    	//callback();
    });
    test('t01', () {
    	xw.startDocument();
    	expect(xw.toString(), equals('<?xml version="1.0"?>\n'));
    });
    test('t02', () {
    	xw.startDocument('1.0', 'utf-8');
    	expect(xw.toString(), equals('<?xml version="1.0" encoding="utf-8"?>\n'));
    });
    test('t03', () {
    	xw.startDocument('1.0', 'utf-8', true);
    	expect(xw.toString(), equals('<?xml version="1.0" encoding="utf-8" standalone="yes"?>\n'));
    });
    test('t04', () {
    	xw.startDocument();
    	xw.endDocument();
    	expect(xw.toString(), equals('<?xml version="1.0"?>\n'));
    });
  });
}