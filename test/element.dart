library xml_writer.test.element;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testElement() {
  group('element', () {
    XMLWriter xw;
    setUp(() {
    	xw = new XMLWriter();
    	//callback();
    });
    test('t01', () {
    	xw.startElement('foo');
    	expect(xw.toString(), equals('<foo/>'));
    });
    test('t02', () {
    	xw.startElement('foo');
    	xw.endElement();
    	expect(xw.toString(), equals('<foo/>'));
    });
    test('t03', () {
    	xw.startElement('foo');
    	xw.text('fake');
    	expect(xw.toString(), equals('<foo>fake</foo>'));
    });
    test('t04', () {
    	xw.startElement('foo');
    	xw.text('fake');
    	xw.endElement();
    	expect(xw.toString(), equals('<foo>fake</foo>'));
    });
    test('t05', () {
    	xw..writeElement('tag', 'value')..endElement();
    	expect(xw.toString(), equals('<tag>value</tag>'));
    });
    test('t06', () {
    	xw.startElement('a');
    	xw.startElement('b');
    	xw.endElement();
    	xw.startElement('c');
    	xw.endElement();
    	xw.endElement();
    	expect(xw.toString(), equals('<a><b/><c/></a>'));
    });
  });
}
