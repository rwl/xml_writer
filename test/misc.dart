library xml_writer.test.misc;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testMisc() {
  group('misc', () {
    XMLWriter xw;
    setUp(() {
    	xw = new XMLWriter();
    	//callback();
    });
//    test('t01', () {
//    	xw..startElement(() { return 'toto'; })..text(() { return 'titi'; });
//    	expect(xw.toString(), equals('<toto>titi</toto>'));
//    });
    test('t02', () {
    	expect(() { xw.startElement('<>'); }, throws);
    });
    test('t03', () {
    	xw..startElement('foo')..text('<toto>');
    	expect(xw.toString(), equals('<foo>&lt;toto&gt;</foo>'));
    });
    test('t03b', () {
    	xw..startElement('foo')..text('un & deux & troie');
    	expect(xw.toString(), equals('<foo>un &amp; deux &amp; troie</foo>'));
    });

    test('t04', () {
    	xw..startElement('foo')..writeAttribute('toto', '"');
    	expect(xw.toString(), equals('<foo toto="&quot;"/>'));
    });
    test('t05', () {
    	xw..startElement('foo')..writeAttribute('toto','&');
    	expect(xw.toString(), equals('<foo toto="&amp;"/>'));
    });
    test('t06', () {
    	xw..startElement('foo')..writeAttribute('toto','&');
    	expect(xw.toString(), equals('<foo toto="&amp;"/>'));
    });
    test('t07', () {
    	xw..startElement('foo')..writeAttribute('toto','"&');
    	expect(xw.toString(), equals('<foo toto="&quot;&amp;"/>'));
    });
    test('t08', () {
    	xw..startElement('foo')..text('<to&to>');
    	expect(xw.toString(), equals('<foo>&lt;to&amp;to&gt;</foo>'));
    });
  });
}





