library xml_writer.test.namespaces;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testNamespaces() {
  group('namespaces', () {
    XMLWriter xw;
    setUp(() {
    	xw = new XMLWriter();
    	//callback();
    });
    test('t01', () {
    	xw.startElement('t:foo');
    	expect(xw.toString(), equals('<t:foo/>'));
    });
    test('t03', () {
    	xw.startElement('foo:tag');
    	xw.text('fake');
    	expect(xw.toString(), equals('<foo:tag>fake</foo:tag>'));
    });
    test('t04', () {
    	xw.startElement('foo:tag');
    	xw.writeAttribute('foo:att', 'value');
    	xw.writeAttribute('att', 'value');
    	xw.text('fake');
    	xw.endElement();
    	expect(xw.toString(), equals('<foo:tag foo:att="value" att="value">fake</foo:tag>'));
    });
    test('t05', () {
    	xw.startDocument('1.0');
    	xw..startElement('rdf:RDF')
    	  ..writeAttribute('xmlns:rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    	  ..writeAttribute('xmlns:cd', 'http://www.recshop.fake/cd#');
    	xw..startElement('rdf:Description')
    	  ..writeAttribute('rdf:about', 'http://www.recshop.fake/cd/Empire Burlesque');
    	xw.writeElement('cd:artist', 'Bob Dylan');
    	xw.writeElement('cd:country', 'USA');
    	xw.writeElement('cd:company', 'Columbia');
    	xw.writeElement('cd:price', '10.90');
    	xw.writeElement('cd:year', '1985');
    	xw.endElement();
    	xw.endElement();
    	xw.endDocument();
    	expect(xw.toString(), equals('<?xml version="1.0"?>\n<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cd="http://www.recshop.fake/cd#"><rdf:Description rdf:about="http://www.recshop.fake/cd/Empire Burlesque"><cd:artist>Bob Dylan</cd:artist><cd:country>USA</cd:country><cd:company>Columbia</cd:company><cd:price>10.90</cd:price><cd:year>1985</cd:year></rdf:Description></rdf:RDF>'));
    });
    // With NS functions
    test('t06', () {
    	xw.startElementNS('t','foo');
    	expect(xw.toString(), equals('<t:foo/>'));
    });

    test('t07', () {
    	xw.startElementNS('foo','tag');
    	xw.text('fake');
    	expect(xw.toString(), equals('<foo:tag>fake</foo:tag>'));
    });
    test('t08', () {
    	xw.startElementNS('foo', 'tag');
    	xw.writeAttributeNS('foo', 'att', null, 'value');
    	xw.writeAttribute('att', 'value');
    	xw.text('fake');
    	xw.endElement();
    	expect(xw.toString(), equals('<foo:tag foo:att="value" att="value">fake</foo:tag>'));
    });
    test('t09', () {
    	xw.startDocument('1.0');
    	xw..startElementNS('rdf','RDF')
    	  ..writeAttributeNS('xmlns','rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    	  ..writeAttributeNS('xmlns','cd', 'http://www.recshop.fake/cd#');
    	xw..startElementNS('rdf','Description')
    	  ..writeAttributeNS('rdf','about', 'http://www.recshop.fake/cd/Empire Burlesque');
    	xw.writeElementNS('cd','artist', null, 'Bob Dylan');
    	xw.writeElementNS('cd','country',null, 'USA');
    	xw.writeElementNS('cd','company', null, 'Columbia');
    	xw.writeElementNS('cd','price', null, '10.90');
    	xw.writeElementNS('cd','year', null,'1985');
    	xw.endElement();
    	xw.endElement();
    	xw.endDocument();
    	expect(xw.toString(), equals('<?xml version="1.0"?>\n<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:cd="http://www.recshop.fake/cd#"><rdf:Description rdf:about="http://www.recshop.fake/cd/Empire Burlesque"><cd:artist>Bob Dylan</cd:artist><cd:country>USA</cd:country><cd:company>Columbia</cd:company><cd:price>10.90</cd:price><cd:year>1985</cd:year></rdf:Description></rdf:RDF>'));
    });
  });
}
