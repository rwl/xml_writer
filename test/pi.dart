library xml_writer.test.pi;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testPI() {
  group('pi', () {
    XMLWriter xw;
    setUp(() {
    	xw = new XMLWriter();
    	//callback();
    });
    test('t01', () {
    	xw.startPI('php');
    	xw.endPI();
    	expect(xw.toString(), equals('<?php?>'));
    });
    test('t02', () {
    	xw.startPI('php');
    	xw.text(' echo');
    	xw.text(' __FILE__; ');
    	xw.endPI();
    	expect(xw.toString(), '<?php echo __FILE__; ?>');
    });
    test('t03', () {
    	xw.startPI('xml-stylesheet');
    	xw.startAttribute('type');
    	xw.text('text/xml');
    	xw.endAttribute();
    	xw.startAttribute('href');
    	xw.text('style.xsl');
    	xw.endAttribute();
    	xw.endPI();
    	expect(xw.toString(), equals('<?xml-stylesheet type="text/xml" href="style.xsl"?>'));
    });
    test('t05', () {
    	xw.writePI('php', ' var_dump(__FILE__); ');
    	expect(xw.toString(), equals('<?php var_dump(__FILE__); ?>'));
    });
  });
}