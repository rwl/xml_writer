library xml_writer.test.raw;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testRaw() {
  group('raw', () {
    XMLWriter xw;
    test('t01', () {
      xw = new XMLWriter( );
      xw.startDocument('1.0', 'UTF-8');
      xw.startElement('foo');
      xw.writeRaw('<one>1</one>');
      xw..startElement('two')..text('2')..endElement();
      xw.endElement();
      xw.endDocument();

      expect(xw.toString(), equals('<?xml version="1.0" encoding="UTF-8"?>\n<foo><one>1</one><two>2</two></foo>'));
    });

    test('t02', () {
      var xw2 = new XMLWriter( );
      xw2.startElement('one');
      xw2.text('1');
      xw2.endElement();

    	xw = new XMLWriter( );
      xw.startDocument('1.0', 'UTF-8');
      xw.startElement('foo');
      xw.writeWriter(xw2);
      xw..startElement('two')..text('2')..endElement();
      xw.endElement();
      xw.endDocument();

      expect(xw.toString(), equals('<?xml version="1.0" encoding="UTF-8"?>\n<foo><one>1</one><two>2</two></foo>'));
    });

    test('t03', () {
      fragment() {
        var xw2 = new XMLWriter();
        xw2.startElement('one');
        xw2.text('1');
        xw2.endElement();
        return xw2.toString();
      }
    	xw = new XMLWriter();
      xw.startDocument('1.0', 'UTF-8');
      xw.startElement('foo');
      xw.writeRaw(fragment());
      xw..startElement('two')..text('2')..endElement();
      xw.endElement();
      xw.endDocument();

      expect(xw.toString(), equals('<?xml version="1.0" encoding="UTF-8"?>\n<foo><one>1</one><two>2</two></foo>'));
    });
  });
}