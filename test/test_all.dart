library xml_writer.test;

import 'attributes.dart';
import 'cdata.dart';
import 'comments.dart';
import 'doctype.dart';
import 'document.dart';
import 'element.dart';
import 'misc.dart';
import 'namespaces.dart';
import 'pi.dart';
import 'raw.dart';
//import 'writer.dart';

main() {
  testAttributes();
  testCData();
  testComments();
  testDocType();
  testDocument();
  testElement();
  testMisc();
  testNamespaces();
  testPI();
  testRaw();
  //testWriter();
}