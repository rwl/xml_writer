library xml_writer.test.writer;

import 'package:unittest/unittest.dart';
import 'package:xml_writer/xml_writer.dart';

testWriter() {
  group('writer', () {
    XMLWriter xw;
    test('t01', () {
    	var filename = '/tmp/foo.xml';
    	var ws = fs.createWriteStream(filename);
    	ws.on('close', () {
    	  expect(fs.readFileSync(filename, 'UTF-8'), equals('<?xml version="1.0" encoding="UTF-8"?>\n<foo>à l\'école !</foo>'));
    	});
    	xw = new XMLWriter(false, (s, e) {
    			ws.write(s, e);
    	});
    	xw..startDocument('1.0', 'UTF-8')..startElement('foo')..text('à l\'école !')..endElement()..endDocument();
    	ws.end();
    });
  });
}
